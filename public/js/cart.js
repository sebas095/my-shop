if (window.location.pathname.includes("/cart")) {
  const btnPay = document.getElementById("pay");
  const update = document.querySelectorAll(".update");
  const remove = document.querySelectorAll(".remove");

  btnPay.addEventListener("click", ev => {
    ev.preventDefault();
    document.querySelector("#clearCart").submit();
  });

  for (let i = 0; i < update.length; i++) {
    update[i].addEventListener("click", function(ev) {
      ev.preventDefault();
      const id = this.id.slice(7);
      const amount = document.querySelector(`#amount-${id}`).value;
      const form = document.querySelector(`#cartForm-${id}`);
      form.amount.value = amount;
      form.submit();
    });
  }

  for (let i = 0; i < remove.length; i++) {
    remove[i].addEventListener("click", function(ev) {
      ev.preventDefault();
      const id = this.id.slice(7);
      if (confirm("¿Estas seguro de eliminar este producto del carrito?")) {
        document.querySelector(`#deleteForm-${id}`).submit();
      }
    });
  }

  function updateInputs() {
    $(".amount").on("keyup mouseup", function() {
      const $amount = $(".amount");
      if (this.value) {
        let total = 0;
        for (let i = 0; i < $amount.length; i++) {
          const id = $($($amount[i])).attr("id").slice(7);
          const price = Number(document.getElementById(`price-${id}`).value);
          total += Number($($($amount[i])).val()) * price;
        }
        $("#total").text(total);
      } else {
        setTimeout(() => {
          if (!this.value) {
            $(this).val(1);
            let total = 0;
            for (let i = 0; i < $amount.length; i++) {
              const id = $($($amount[i])).attr("id").slice(7);
              const price = Number(
                document.getElementById(`price-${id}`).value
              );
              total += Number($($($amount[i])).val()) * price;
            }
            $("#total").text(total);
          }
        }, 1000);
      }
    });
  }

  updateInputs();
}
