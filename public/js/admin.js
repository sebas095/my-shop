if (window.location.pathname.includes("/products/menu")) {
  const del = document.querySelectorAll(".delete");
  const add = document.querySelectorAll(".add");

  for (let i = 0; i < del.length; i++) {
    del[i].addEventListener("click", function(ev) {
      ev.preventDefault();
      const id = this.id.slice(7);
      if (confirm("¿Estas seguro que deseas Eliminar este producto?")) {
        document.querySelector(`#deleteProduct-${id}`).submit();
      }
    });
  }

  for (let i = 0; i < add.length; i++) {
    add[i].addEventListener("click", function(ev) {
      ev.preventDefault();
      const id = this.id.slice(4);
      document.querySelector(`#form-${id}`).submit();
    });
  }
}
