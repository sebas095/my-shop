if (window.location.pathname.includes("/products")) {
  const btnDelete = document.getElementById("delete");
  btnDelete.addEventListener("click", ev => {
    ev.preventDefault();
    if (confirm("¿Estas seguro que deseas Eliminar este producto?")) {
      document.getElementById("deleteProduct").submit();
    }
  });
}
