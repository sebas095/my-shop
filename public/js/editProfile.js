if (window.location.pathname.includes("/profile")) {
  // Form
  const form = document.getElementById("profile");
  const inputs = form.elements;

  // Buttons
  const btnEdit = document.getElementById("edit");

  // Disable inputs
  for (let i = 0; i < inputs.length; i++) {
    if (!inputs[i].id.includes("edit")) {
      inputs[i].disabled = true;
    }
  }

  btnEdit.addEventListener("click", ev => {
    ev.preventDefault();
    for (let i = 0; i < inputs.length; i++) {
      if (!inputs[i].id.includes("edit")) {
        inputs[i].disabled = !inputs[i].disabled;
      }
    }
  });
}
