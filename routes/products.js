const express = require("express");
const router = express.Router();
const { productController } = require("../controllers");

module.exports = (app, mountPoint) => {
  // GET
  router.get("/", productController.listProducts);
  router.get("/new", productController.new);
  router.get("/search", productController.search);
  router.get("/menu", productController.menu);
  router.get("/:id", productController.getProduct);

  // POST
  router.post("/new", productController.create);

  // PUT
  router.put("/:id", productController.editProduct);

  // DELETE
  router.delete("/:id", productController.deleteProduct);

  app.use(mountPoint, router);
};
