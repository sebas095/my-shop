const express = require("express");
const router = express.Router();
const { userController, sessionController } = require("../controllers");

module.exports = (app, mountPoint) => {
  // GET
  router.get("/register", userController.newUser);
  router.get(
    "/:id/cart",
    sessionController.loginRequired,
    userController.getCart
  );

  // POST
  router.post("/register", userController.createUser);
  router.post(
    "/:id/cart",
    sessionController.loginRequired,
    userController.addToCart
  );

  // PUT
  router.put(
    "/:id/cart/:productId",
    sessionController.loginRequired,
    userController.editCartItems
  );

  // DELETE
  router.delete(
    "/:id/cart/:productId",
    sessionController.loginRequired,
    userController.removeFromCart
  );

  router.delete(
    "/:id/cart",
    sessionController.loginRequired,
    userController.clearCart
  );

  app.use(mountPoint, router);
};
