const express = require("express");
const router = express.Router();
const { sessionController } = require("../controllers");

module.exports = (app, mountPoint, passport) => {
  // GET
  router.get("/login", sessionController.new);
  router.get("/new-password", sessionController.newPassword);
  router.get("/reset-password/:token", sessionController.resetPassword);

  // POST
  router.post("/email-recovery", sessionController.sendEmail);
  router.post(
    "/login",
    passport.authenticate("local-login", {
      successRedirect: "/products",
      failureRedirect: "/session/login",
      failureFlash: true
    })
  );

  // PUT
  router.put("/reset-password/:token", sessionController.changePassword);

  // DELETE
  router.delete("/logout", sessionController.destroy);

  app.use(mountPoint, router);
};
