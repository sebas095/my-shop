# my-shop

## Requeriments/dependencies
* [NodeJS](https://nodejs.org/en/)
* [Mongodb](https://docs.mongodb.com/manual/installation/)

## Installation
```bash
npm install or yarn install
```

## Run
```bash
npm start or yarn start
```
Open in your browser: [http://localhost:3000/](http://localhost:3000/)
