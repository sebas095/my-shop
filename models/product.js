const mongoose = require("mongoose");
const timestamp = require("mongoose-timestamp");
const { Schema } = mongoose;

const ProductSchema = new Schema({
  title: {
    type: String,
    require: true
  },
  price: {
    type: Number,
    require: true
  },
  image: {
    type: String,
    default: "",
    require: true
  }
});

ProductSchema.plugin(timestamp);
ProductSchema.pre("save", function(next) {
  this.title = this.title.toLowerCase();
  next();
});

module.exports = mongoose.model("Product", ProductSchema);
