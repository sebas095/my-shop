module.exports = {
  userController: require("./user"),
  sessionController: require("./session"),
  productController: require("./product")
};
