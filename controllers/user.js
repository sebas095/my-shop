const User = require("../models/user");
const Product = require("../models/product");
const _ = require("underscore");

// GET /users/register -- Register form
exports.newUser = (req, res) => {
  if (!req.isAuthenticated()) {
    res.render("users/new", { message: req.flash("registerMessage") });
  } else {
    res.redirect("/profile");
  }
};

// POST /users/register -- Create a new user
exports.createUser = (req, res) => {
  if (req.body.password !== req.body.cpassword) {
    req.flash("registerMessage", "Las contraseñas no coinciden");
    return res.redirect("/users/register");
  }
  User.find({}, (err, users) => {
    if (err) {
      console.log("Error: ", err);
      req.flash(
        "homeMessage",
        "Hubo problemas en el registro, intenta de nuevo"
      );
      return res.redirect("/");
    } else if (users.length === 0) {
      req.body.state = "1";
    } else {
      req.body.state = "2";
    }

    User.create(req.body, (err, user) => {
      if (err) {
        console.log("Error: ", err);
        req.flash(
          "homeMessage",
          "Hubo problemas en el registro, intenta de nuevo"
        );
        return res.redirect("/");
      }
      if (req.body.state === "1") {
        req.flash(
          "loginMessage",
          "La cuenta ha sido creada exitosamente y tienes permisos de administrador"
        );
      } else {
        req.flash("loginMessage", "La cuenta ha sido creada exitosamente");
      }
      res.redirect("/session/login");
    });
  });
};

// PUT /profile -- Modifies user data
exports.updateProfile = (req, res) => {
  const id = req.user._id;

  User.findByIdAndUpdate(id, req.body, { new: true }, (err, user) => {
    if (err) {
      console.log("Error: ", err);
      req.flash(
        "homeMessage",
        "Hubo problemas actualizando los datos, intenta de nuevo"
      );
      return res.redirect("/");
    }
    req.flash("userMessage", "Sus datos han sido actualizados exitosamente");
    res.redirect("/profile");
  });
};

// POST /users/:id/cart -- Add a product in cart
exports.addToCart = (req, res) => {
  const { productId } = req.body;
  User.findById(req.user.id, (err, user) => {
    if (err) {
      console.log("Error: ", err);
      req.flash(
        "homeMessage",
        "Hubo problemas para añadir al carrito, intenta de nuevo"
      );
      return res.redirect("/products");
    } else {
      let { productCart } = user;
      const index = _.findIndex(productCart, {
        product: productId
      });
      if (index !== -1) {
        productCart[index].amount += 1;
      } else {
        productCart.push({
          amount: 1,
          product: productId
        });
      }

      User.findByIdAndUpdate(
        req.user.id,
        { productCart },
        { new: true },
        (err, data) => {
          if (err) {
            console.log("Error: ", err);
            req.flash(
              "productListMessage",
              "Hubo problemas para añadir al carrito, intenta de nuevo"
            );
            return res.redirect("/products");
          } else {
            req.flash(
              "productListMessage",
              "El producto ha sido añadido al carrito exitosamente"
            );
            res.redirect("/products");
          }
        }
      );
    }
  });
};

// GET /users/:id/cart -- Show cart of a specific users
exports.getCart = (req, res) => {
  User.findById(req.user._id, (err, user) => {
    if (err) {
      console.log("Error: ", err);
      req.flash(
        "productListMessage",
        "Hubo problemas para abrir el carrito, intenta de nuevo"
      );
      return res.redirect("/products");
    } else {
      let { productCart } = user;
      if (productCart.length > 0) {
        let products = productCart.map(item => item.product);
        Product.find({ _id: { $in: products } }, (err, data) => {
          if (err) {
            console.log("Error: ", err);
            req.flash(
              "productListMessage",
              "Hubo problemas para abrir el carrito, intenta de nuevo"
            );
            return res.redirect("/products");
          }
          for (let i = 0; i < data.length; i++) {
            const index = _.findIndex(productCart, {
              product: data[i]._id.toString()
            });
            productCart[index].product = data[i];
          }
          const total = productCart.reduce((total, item) => {
            return total + item.amount * item.product.price;
          }, 0);
          res.render("users/cart", {
            productCart,
            total,
            message: req.flash("cartMessage")
          });
        });
      } else {
        res.render("users/cart", {
          productCart: [],
          message: "No hay productos en el carrito"
        });
      }
    }
  });
};

// DELETE /users/:id/cart/:productId -- Remove a product from cart
exports.removeFromCart = (req, res) => {
  User.findById(req.user._id, (err, user) => {
    if (err) {
      console.log("Error: ", err);
      req.flash(
        "cartMessage",
        "Hubo problemas para eliminar productos del carrito, intenta de nuevo"
      );
      return res.redirect(`/users/${req.user._id}/cart`);
    } else {
      let { productCart } = user;
      const index = _.findIndex(productCart, {
        product: req.params.productId
      });
      productCart.splice(index, 1);
      User.findByIdAndUpdate(
        req.user._id,
        { productCart },
        { new: true },
        (err, usr) => {
          if (err) {
            console.log("Error: ", err);
            req.flash(
              "cartMessage",
              "Hubo problemas para eliminar productos del carrito, intenta de nuevo"
            );
            return res.redirect(`/users/${req.user._id}/cart`);
          } else {
            req.flash(
              "cartMessage",
              "Los datos del carrito han sido actualizados exitosamente"
            );
            return res.redirect(`/users/${req.user._id}/cart`);
          }
        }
      );
    }
  });
};

// PUT /users/:id/cart/:productId -- Modifies a specific product from cart
exports.editCartItems = (req, res) => {
  const { amount } = req.body;
  User.findById(req.user._id, (err, user) => {
    if (err) {
      console.log("Error: ", err);
      req.flash(
        "cartMessage",
        "Hubo problemas actualizar el producto del carrito, intenta de nuevo"
      );
      return res.redirect(`/users/${req.user._id}/cart`);
    } else {
      let { productCart } = user;
      const index = _.findIndex(productCart, {
        product: req.params.productId
      });
      productCart[index].amount = amount;
      User.findByIdAndUpdate(
        req.user._id,
        { productCart },
        { new: true },
        (err, usr) => {
          if (err) {
            console.log("Error: ", err);
            req.flash(
              "cartMessage",
              "Hubo problemas actualizar el producto del carrito, intenta de nuevo"
            );
            return res.redirect(`/users/${req.user._id}/cart`);
          } else {
            req.flash(
              "cartMessage",
              "Los datos del carrito han sido actualizados exitosamente"
            );
            return res.redirect(`/users/${req.user._id}/cart`);
          }
        }
      );
    }
  });
};

// DELETE /users/:id/cart -- Clear products of the cart
exports.clearCart = (req, res) => {
  User.findByIdAndUpdate(
    req.user._id,
    { productCart: [] },
    { new: true },
    (err, usr) => {
      if (err) {
        console.log("Error: ", err);
        req.flash(
          "cartMessage",
          "Hubo problemas finalizar la compra, intenta de nuevo"
        );
        return res.redirect(`/users/${req.user._id}/cart`);
      } else {
        req.flash(
          "productListMessage",
          "La compra ha sido realizada exitosamente"
        );
        return res.redirect("/products");
      }
    }
  );
};
