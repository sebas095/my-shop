const Product = require("../models/product");
const multer = require("multer");
const path = require("path");
const { imageFilter, renameFile, deleteFile } = require("../helpers");
const upload = multer({
  dest: "public/uploads",
  fileFilter: imageFilter
}).single("image");

// GET /products/menu -- Show a menu for an Admin
exports.menu = (req, res) => {
  if (req.user.state === "1") {
    res.render("products/menu");
  } else {
    req.flash("hoMessage", "No tienes permisos para acceder");
    res.redirect("/");
  }
};

// POST /products/new -- Create a new product
exports.create = (req, res) => {
  if (req.user.state === "1") {
    upload(req, res, err => {
      if (err) {
        console.log("Error: ", err);
        req.flash(
          "homeMessage",
          "Hubo problemas guardando el producto, intenta de nuevo"
        );
        res.redirect("/");
      } else {
        const product = new Product({
          title: req.body.title.toLowerCase(),
          price: req.body.price,
          image: "/img/default.jpg"
        });
        if (req.file) {
          const dir = __dirname + "/../public/uploads";
          const oldName = req.file.filename;
          const ext = path.extname(req.file.originalname);
          const newName = product._id + ext;

          renameFile(dir, oldName, newName, (err, filename) => {
            product.image = `/uploads/${filename}`;
            product.save(err => {
              if (err) {
                console.log(err);
                req.flash(
                  "homeMessage",
                  "Hubo problemas guardando el producto, intenta de nuevo"
                );
                res.redirect("/");
              } else {
                req.flash(
                  "productMessage",
                  "EL producto ha sido creado exitosamente"
                );
                res.redirect(`/products/${product._id}`);
              }
            });
          });
        } else {
          product.save(err => {
            if (err) {
              console.log(err);
              req.flash(
                "homeMessage",
                "Hubo problemas guardando el producto, intenta de nuevo"
              );
              res.redirect("/");
            } else {
              req.flash(
                "productMessage",
                "EL producto ha sido creado exitosamente"
              );
              res.redirect(`/products/${product._id}`);
            }
          });
        }
      }
    });
  } else {
    req.flash("homeMessage", "No tienes permisos para acceder");
    res.redirect("/");
  }
};

// GET /products -- List products
exports.listProducts = (req, res) => {
  Product.find({}, null, { sort: { createdAt: -1 } }, (err, products) => {
    if (err) {
      console.log(err);
      req.flash(
        "homeMessage",
        "Hubo problemas obteniendo los datos de las productos intenta de nuevo"
      );
      res.redirect("/");
    } else if (products.length > 0) {
      res.render("products/list", {
        products,
        message: req.flash("productListMessage")
      });
    } else {
      req.flash("homeMessage", "No hay productos disponibles");
      res.redirect("/");
    }
  });
};

// GET /products/new -- Product form
exports.new = (req, res) => {
  res.render("products/new");
};

// GET /products/:id -- Return a specific product
exports.getProduct = (req, res) => {
  const { id } = req.params;
  Product.findById(id, (err, product) => {
    if (err) {
      console.log("Error: ", err);
      req.flash(
        "homeMessage",
        "Hubo problemas buscando el producto, intenta más tarde"
      );
      res.redirect("/");
    } else if (!product) {
      req.flash(
        "homeMessage",
        "El producto solicitado no se encuentra registrado"
      );
      res.redirect("/");
    } else {
      res.render("products/edit", {
        product,
        message: req.flash("productMessage")
      });
    }
  });
};

// PUT /product/:id -- Modifies a specific product
exports.editProduct = (req, res) => {
  if (req.user.state === "1") {
    upload(req, res, err => {
      if (err) {
        console.log("Error: ", err);
        req.flash(
          "homeMessage",
          "Hubo problemas editando el producto, intenta de nuevo"
        );
        res.redirect("/");
      } else {
        const { id } = req.params;
        let product = {
          title: req.body.title.toLowerCase(),
          price: req.body.price
        };

        if (req.file) {
          const dir = __dirname + "/../public/uploads";
          const oldName = req.file.filename;
          const ext = path.extname(req.file.originalname);
          const newName = id + ext;

          renameFile(dir, oldName, newName, (err, filename) => {
            product.image = `/uploads/${filename}`;
            Product.findByIdAndUpdate(
              id,
              product,
              { new: true },
              (err, data) => {
                if (err) {
                  console.log(err);
                  req.flash(
                    "homeMessage",
                    "Hubo problemas actualizando los datos, intenta más tarde"
                  );
                  return res.redirect("/");
                }
                req.flash(
                  "productMessage",
                  "Los datos han sido actualizados exitosamente"
                );
                res.redirect(`/products/${id}`);
              }
            );
          });
        } else {
          Product.findByIdAndUpdate(id, product, { new: true }, (err, data) => {
            if (err) {
              console.log(err);
              req.flash(
                "homeMessage",
                "Hubo problemas actualizando los datos, intenta más tarde"
              );
              return res.redirect("/");
            }
            req.flash(
              "productMessage",
              "Los datos han sido actualizados exitosamente"
            );
            res.redirect(`/products/${id}`);
          });
        }
      }
    });
  } else {
    req.flash("homeMessage", "No tienes permisos para acceder");
    res.redirect("/");
  }
};

// DELETE /product/:id -- Delete a specific product
exports.deleteProduct = (req, res) => {
  if (req.user.state === "1") {
    const { id } = req.params;
    Product.findByIdAndRemove(id, (err, data) => {
      if (err) {
        console.log(err);
        req.flash(
          "homeMessage",
          "Hubo problemas para eliminar el producto, intenta más tarde"
        );
        res.redirect("/");
      } else {
        deleteFile(data.image, err => {
          if (err) {
            req.flash(
              "homeMessage",
              "Hubo problemas para eliminar el producto, intenta más tarde"
            );
            return res.redirect("/");
          }
          req.flash(
            "homeMessage",
            "El producto ha sido eliminado exitosamente"
          );
          res.redirect("/");
        });
      }
    });
  } else {
    req.flash("homeMessage", "No tienes permisos para acceder");
    res.redirect("/");
  }
};

// GET /products/search -- Form to search a specific product
exports.search = (req, res) => {
  if (req.user.state === "1") {
    if (req.query.title) {
      Product.find(
        { title: req.query.title.toLowerCase() },
        null,
        {
          sort: { createdAt: -1 }
        },
        (err, products) => {
          if (err) {
            console.log("Error: ", err);
            res.render("products/search", {
              message: "Hubo problemas buscando el producto, intenta de nuevo",
              products: [],
              results: []
            });
          } else if (products.length > 0) {
            res.render("products/search", {
              message: "",
              products,
              results: []
            });
          } else {
            res.render("products/search", {
              message: "No hay resultados encontrados",
              products: [],
              results: []
            });
          }
        }
      );
    } else {
      Product.find({}, null, { sort: { createdAt: -1 } }, (err, products) => {
        if (err) {
          console.log("Error: ", err);
          res.render("products/search", {
            message: "Hubo problemas en el servidor, intenta de nuevo",
            products: [],
            results: []
          });
        } else if (products.length > 0) {
          res.render("products/search", {
            message: "",
            products: [],
            results: products.slice(0, 10)
          });
        } else {
          res.render("products/search", {
            message: "No hay productos disponibles",
            products: [],
            results: []
          });
        }
      });
    }
  } else {
    req.flash("homeMessage", "No tienes permisos para acceder");
    res.redirect("/");
  }
};
