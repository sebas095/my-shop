const fs = require("fs");

exports.imageFilter = (req, file, cb) => {
  // accept image only
  if (!file.originalname.toLowerCase().match(/\.(jpg|jpeg|png|gif)$/)) {
    return cb(new Error("Only image files are allowed!"), false);
  }
  cb(null, true);
};

exports.renameFile = (path, oldName, newName, cb) => {
  fs.rename(`${path}/${oldName}`, `${path}/${newName}`, err => {
    if (err) return cb(err, null);
    cb(null, newName);
  });
};

exports.deleteFile = (path, cb) => {
  const dir = __dirname + "/../public" + path;
  fs.unlink(dir, err => {
    if (err) return cb(err);
    return cb();
  });
};
